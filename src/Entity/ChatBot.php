<?php

namespace Drupal\chatbots_api\Entity;

use Drupal\chatbots_api\ChatBotsApiEntityInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines general ChatBot Entity.
 *
 * @ConfigEntityType(
 *   id = "chatbot",
 *   label = @Translation("Chat Bot"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\chatbots_api\Form\ChatBotForm",
 *       "edit" = "Drupal\chatbots_api\Form\ChatBotForm",
 *       "delete" = "Drupal\chatbots_api\Form\ChatBotDeleteForm",
 *     },
 *     "list_builder" = "Drupal\chatbots_api\Controller\ChatBotListBuilder"
 *   },
 *   config_prefix = "chatbot",
 *   admin_permission = "administer chatbots",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "description" = "description",
 *     "type" = "type",
 *     "data" = "data"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/chatbots/{chatbot}",
 *     "delete-form" = "/admin/structure/chatbots/{chatbot}/delete",
 *   }
 * )
 */
class ChatBot extends ConfigEntityBase implements ChatBotsApiEntityInterface {

  /**
   * Chat bot description.
   *
   * @var string
   */
  protected $description;

  /**
   * Chat bot type.
   *
   * @var string
   */
  protected $type;

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    if (isset($this->{$property_name})) {
      return $this->{$property_name};
    }

    // Looking to serialized data.
    return $this->getData($property_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($property_name = NULL) {
    $data = Json::decode($this->get('data'));

    if ($property_name === NULL) {
      return $data;
    }

    return isset($data[$property_name]) ? $data[$property_name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setData($data) {
    return $this->set('data', Json::encode($data));
  }

}
