<?php

namespace Drupal\chatbots_api;

use Drupal\Core\Plugin\PluginBase;

/**
 * Class ChatBotsBotBase.
 *
 * TODO: Add abstract methods with documentation.
 */
abstract class ChatBotsBotBase extends PluginBase implements ChatBotsApiInterface {}
