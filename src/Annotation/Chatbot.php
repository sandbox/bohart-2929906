<?php

namespace Drupal\chatbots_api\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Chatbot annotation object.
 *
 * Plugin Namespace: Plugin\Chatbot.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Chatbot extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the Chatbot plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the mail plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
