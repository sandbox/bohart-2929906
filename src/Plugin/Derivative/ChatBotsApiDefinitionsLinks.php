<?php

namespace Drupal\chatbots_api\Plugin\Derivative;

use Drupal\chatbots_api\ChatBotsApiBotManager;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ChatBotsApiDefinitionsLinks.
 */
class ChatBotsApiDefinitionsLinks extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * Chatbot plugin manager.
   *
   * @var \Drupal\chatbots_api\ChatBotsApiBotManager
   */
  protected $chatbotManager;

  /**
   * ChatBotsApiTypesLinks constructor.
   */
  public function __construct(ChatBotsApiBotManager $chatbotManager) {
    $this->chatbotManager = $chatbotManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.chatbots_api.bot_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $actions = [];

    foreach ($this->chatbotManager->getDefinitions() as $definition) {
      $actions[$definition['id']] = [
        'title' => $this->t('Add @label', ['@label' => $definition['label']]),
        'route_parameters' => [
          'chatbot_type' => $definition['id'],
        ],
      ] + $base_plugin_definition;
    }
    return $actions;
  }

}
