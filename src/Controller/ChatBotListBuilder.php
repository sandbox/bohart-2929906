<?php

namespace Drupal\chatbots_api\Controller;

use Drupal\chatbots_api\ChatBotsApiBotManager;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ChatBotsListBuilder.
 */
class ChatBotListBuilder extends ConfigEntityListBuilder {

  /**
   * Chatbot plugin manager.
   *
   * @var \Drupal\chatbots_api\ChatBotsApiBotManager
   */
  protected $chatbotManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ChatBotsApiBotManager $chatbotManager) {
    parent::__construct($entity_type, $storage);
    $this->chatbotManager = $chatbotManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.chatbots_api.bot_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Name'),
      'type' => $this->t('Type'),
      'description' => $this->t('Description'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * TODO: Tell that our entity implements ChatBotsApiEntityInterface.
   */
  public function buildRow(EntityInterface $entity) {

    return [
      'label' => $entity->label(),
      'type' => $entity->getType(),
      'description' => $entity->getDescription(),
    ] + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    if ($this->chatbotManager->hasImplementations()) {
      return $build;
    }

    // @TODO: Prepare list of modules. Probably move to the helper function.
    return [
      'warning' => [
        '#theme' => 'item_list',
        '#title' => $this->t('Please install one of the child modules:'),
        '#items' => [
          Link::fromTextAndUrl($this->t('Messenger Bot API'), Url::fromUri('https://www.drupal.org/sandbox/bohart/2929907'))->toString(),
        ],
      ],
    ] + $build;
  }

}
