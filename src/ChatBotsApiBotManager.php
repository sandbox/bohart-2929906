<?php

namespace Drupal\chatbots_api;

use Drupal\Core\Plugin\DefaultPluginManager;
use \Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class ChatBotsApiBotManager.
 */
class ChatBotsApiBotManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Chatbot', $namespaces, $module_handler, 'Drupal\chatbots_api\ChatBotsApiInterface', 'Drupal\chatbots_api\Annotation\Chatbot');

    // TODO: alter and set cache (if needed).
  }

  /**
   * Helper function to check if we have at least one implementation.
   */
  public function hasImplementations() {
    return !empty($this->getDefinitions());
  }

}
