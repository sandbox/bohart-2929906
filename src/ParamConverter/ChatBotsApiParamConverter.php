<?php

namespace Drupal\chatbots_api\ParamConverter;

use Drupal\chatbots_api\ChatBotsApiBotManager;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Class ChatBotsApiParamConverter.
 */
class ChatBotsApiParamConverter implements ParamConverterInterface {

  /**
   * Chatbot plugin manager.
   *
   * @var \Drupal\chatbots_api\ChatBotsApiBotManager
   */
  protected $chatbotManager;

  /**
   * ChatBotsApiParamConverter constructor.
   */
  public function __construct(ChatBotsApiBotManager $chatbotManager) {
    $this->chatbotManager = $chatbotManager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if ($this->chatbotManager->hasDefinition($value)) {
      return $this->chatbotManager->createInstance($value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    if (empty($definition['type']) || $definition['type'] !== 'chatbot_type') {
      return FALSE;
    }
    return in_array($definition['type'], $route->compile()->getVariables(), TRUE);
  }

}
