<?php

namespace Drupal\chatbots_api;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ChatBotsInterface.
 */
interface ChatBotsApiEntityInterface extends ConfigEntityInterface {

  /**
   * Returns chat bot description.
   *
   * @return string
   *   Description text.
   */
  public function getDescription();

  /**
   * Returns chat bot type.
   *
   * @return string
   *   Chat bot type label.
   */
  public function getType();

}
