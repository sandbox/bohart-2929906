<?php

namespace Drupal\chatbots_api\Access;

use Drupal\chatbots_api\ChatBotsApiBotManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Class ChatbotsApiAccess.
 */
class ChatbotsApiAccess implements AccessInterface {

  /**
   * Chatbot plugin manager.
   *
   * @var \Drupal\chatbots_api\ChatBotsApiBotManager
   */
  protected $chatbotManager;

  /**
   * ChatbotsApiAccess constructor.
   */
  public function __construct(ChatBotsApiBotManager $chatBotManager) {
    $this->chatbotManager = $chatBotManager;
  }

  /**
   * Check access.
   */
  public function access() {
    return AccessResult::allowedIf($this->chatbotManager->hasImplementations());
  }

}
