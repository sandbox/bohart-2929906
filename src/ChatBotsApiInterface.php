<?php

namespace Drupal\chatbots_api;

use Drupal\chatbots_api\Entity\ChatBot;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface ChatBotsInterface.
 *
 * TODO: Here we will require methods for chatbot implementations.
 */
interface ChatBotsApiInterface extends PluginInspectionInterface {

  /**
   * Method to extend basic chat bot form.
   *
   * Can be used to add additional fields that will help to store required data.
   *
   * @return array
   *   Form array.
   */
  public function extendForm(array $form, FormStateInterface $form_state, ChatBot $entity);

}
