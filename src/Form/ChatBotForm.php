<?php

namespace Drupal\chatbots_api\Form;

use Drupal\chatbots_api\ChatBotsApiBotManager;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ChatBotForm.
 */
class ChatBotForm extends EntityForm {

  /**
   * Chatbot plugin manager.
   *
   * @var \Drupal\chatbots_api\ChatBotsApiBotManager
   */
  protected $chatBotManager;

  /**
   * Chatbot instance.
   *
   * @var \Drupal\chatbots_api\ChatBotsApiInterface
   */
  protected $chatbotType;

  /**
   * Specific form elements provided by chatbot plugin.
   *
   * @var array
   */
  private $specificElements;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.chatbots_api.bot_manager')
    );
  }

  /**
   * ChatBotForm constructor.
   */
  public function __construct(ChatBotsApiBotManager $chatBotManager) {
    $this->chatBotManager = $chatBotManager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\chatbots_api\Entity\ChatBot $bot */
    $bot = $this->entity;

    $form['label'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#default_value' => $bot->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $bot->id(),
      '#machine_name' => [
        'exists' => ['Drupal\chatbots_api\Entity\ChatBot', 'load'],
        'source' => ['label'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$bot->isNew(),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $bot->getDescription(),
    ];

    // Remember basic form without specific elements.
    $origin_form = $form;

    // Add specific elements.
    $form = $this->chatbotType->extendForm($form, $form_state, $bot);

    // Fetch and save specific elements to make possible store them in
    // serialized data field.
    $this->specificElements = array_keys(array_diff_key($form, $origin_form));

    $form['#entity_builders'][] = '::addChatbotValues';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $chatbot_type = NULL) {
    $this->chatbotType = $chatbot_type;

    // On edit page we can get chat bot type from entity object.
    if ($this->chatbotType === NULL && $this->entity !== NULL) {
      $this->chatbotType = $this->chatBotManager->createInstance($this->entity->getType());
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Set chat bot type and specific values.
   */
  public function addChatbotValues($entity_type, $entity, &$form, $form_state) {
    $entity->set('type', $this->chatbotType->getPluginId());

    // Add specific values provided by chat bot type implementation.
    $specific_elements_to_save = array_intersect_key($form_state->getValues(), array_flip($this->specificElements));

    $entity->set('data', Json::encode($specific_elements_to_save));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $bot = $this->entity;
    $status = $bot->save();

    if ($status) {
      $message = $this->t('Saved %label settings.', ['%label' => $bot->label()]);
    }
    else {
      $message = $this->t('%label settings was not saved.', ['%label' => $bot->label()]);
    }

    drupal_set_message($message);
    $form_state->setRedirect('entity.chatbot.collection');
  }

}
